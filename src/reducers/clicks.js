
//Clicks Reducer
export default function clicks(state = {}, action) {
    switch (action.type) {
      case 'CLICKS_LIST_SAVE':
        return action.clicks;
      // initial state
      default:
        return state;
    }
  }