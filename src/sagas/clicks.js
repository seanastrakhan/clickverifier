import { call, put } from "redux-saga/effects";
import ApiClicks from "../api/clicks";

// fetch the clicks's list
export function* clicksFetchList(action) {
  const clicks = yield call(ApiClicks.getList);

  yield put({
    type: 'CLICKS_LIST_SAVE',
    clicks: clicks,
  });
}