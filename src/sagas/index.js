// main saga generators
import { takeLatest } from "redux-saga";
import { fork } from "redux-saga/effects";
import {clicksFetchList} from "./clicks"

export function* sagas() {
  yield [
    fork(takeLatest, 'CLICKS_FETCH_LIST', clicksFetchList)
  ];
}
