import React, {Component} from 'react';

class ClickDisplay extends Component {
    render() {
        return (
            <div>
                <h5>IP: {this.props.click.ip}</h5>
                <ul>
                    <li>Time: {this.props.click.timestamp}</li>
                    <li>Amount: {this.props.click.amount}</li>
                </ul>
            </div>
        );
    }
}

export default ClickDisplay;