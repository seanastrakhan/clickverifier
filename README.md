# Clicks Coding Challenge

A simple React/Redux application to solve this challenge:

Given an array of clicks, return the subset of clicks where:

1. For each IP within each one hour period, only the most expensive click is placed into the result set.
2. If more than one click from the same IP ties for the most expensive click in a one hour period, only
place the earliest click into the result set.
3. If there are more than 10 clicks for an IP in the overall array of clicks, do not include any clicks from that
IP address in the result set.    

## Architectural Overview

To imitate a real-world example of this coding challenge I envisioned the clicks JSON would come from some sort of API hence I hard-coded the clicks and mock asynchronous logic to fetch it (src -> api).  

Since the filtering aspect is not UI related I abstracted that logic into a middleware component that uses a ClickMonitor object to handle the business rules (src -> middleware).  

### Dependencies

You need Node on your machine to run and the project uses the following external packages.

```
Moment.JS (Handle date logic)
Ramda.JS (Handle querying)
```

### Installing

The requirements asked for the results to be output to a file.  However, since I was told I could use React I only have access to the browser and not the file server.
Therefore, my solution does not use the command "npm run solution" but instead executes via "npm start" and renders the results to the browser.

To run the project for the first time, execute the following in the root folder. 

```
npm install
npm start
```

Navigate to localhost:8080 to see results.

### Running the tests

To run tests execute the following:

```
npm test
```

## Design Decisions

The primary obstacle of this challenge that kept it from being a standard iterative solution, was the exception of not including IPs if they occurred more than 10 times.  

My original thought was to iterate over the clicks set adhering to the time/amount rules and then iterate again counting and removing any IPs from the result set that were greater than 10.  This approach is not as performant given the unnecessary iterations.

The approach I selected was to initially create a list of Blacklisted IPs and checking to see if a given IP was in that list prior to processing it.  The ideal solution would be to not create a list of Blacklisted IPs, but instead remove any blacklisted IPs from the original data set.  This approach would save memory and iterative cycles.     

The Ramda.js library offers a GroupBy option that can help in this process; however, it returns an object instead of an array hence making the removal of unwanted IPs slightly more tedious.  For the sake of time I settled on making this separate list of Blacklisted IPs and querying it during iteration. 


### Ramda & Moment

Moment.js was chosen to simplify dealing with timestamps.  The original approach used the Date object but it quickly became verbose to deal with. 

```
var date = new Date(timeStamp);
let minuteAmount = date.getMinutes();
this.currHourStart = new Date(date.getTime() - (minuteAmount * 60000));
this.nextHourStart = new Date(this.currHourStart.getTime() + (60 * 60000));
```

Ramda.js was selected due to its convenient methods to query JS objects and arrays to avoid lengthy native methods and logic like the Map function. 
