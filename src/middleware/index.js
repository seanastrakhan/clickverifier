import ClickMonitor from "./clickMonitor"


function verifyClicksMiddleware({dispatch}){
    return function(next){
        return function(action){

           if(action.type === 'CLICKS_LIST_SAVE'){
                
                //Verify if clicks meet coding challenge criteria
                let clickMonitor = new ClickMonitor(10);
                let filteredClicks = clickMonitor.processClicks(action.clicks);
                action.clicks = filteredClicks;
            }

            return next(action);
        }
    }
}

export default verifyClicksMiddleware;