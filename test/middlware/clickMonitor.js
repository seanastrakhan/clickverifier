import assert from "assert";
import ClickMonitor from "../../src/middleware/clickMonitor"
import clicksFetchList from "../../src/sagas/clicks";

describe('Click Monitor', () =>{
    describe('verifyClickMonitor()', () =>{

        let clickMonitor = new ClickMonitor(10);

        ///////////  Test 1: Ensure highest amount for that hour is selected  /////////////
        let testForHighestPerHourClicks = [
            { "ip":"22.22.22.22", "timestamp":"3/11/2016 02:02:58", "amount": 7.00 },
            { "ip":"11.11.11.11", "timestamp":"3/11/2016 02:12:32", "amount": 6.50 },
            { "ip":"11.11.11.11", "timestamp":"3/12/2016 02:13:11", "amount": 9.25 },
            { "ip":"11.11.11.11", "timestamp":"3/12/2016 02:15:11", "amount": 2.25 },
            { "ip":"44.44.44.44", "timestamp":"3/13/2016 03:13:54", "amount": 8.75 }
        ];

        let expectedResultHighestAmount = [
            { "ip":"22.22.22.22", "timestamp":"3/11/2016 02:02:58", "amount": 7.00 },
            { "ip":"11.11.11.11", "timestamp":"3/12/2016 02:13:11", "amount": 9.25 },
            { "ip":"44.44.44.44", "timestamp":"3/13/2016 03:13:54", "amount": 8.75 }
        ]

        let resultHighestAmount = clickMonitor.processClicks(testForHighestPerHourClicks)


        it('Ensure highest amount for that hour is selected', () => {
            assert.deepEqual(expectedResultHighestAmount, resultHighestAmount);
        });

        ///////////  Test 2: Ensure IP counts greater than 10 are excluded  ////////////
        let testClickLimit = [
            { "ip":"22.22.22.22", "timestamp":"3/11/2016 02:02:58", "amount": 7.00 },
            { "ip":"11.11.11.11", "timestamp":"3/11/2016 02:12:32", "amount": 6.50 },
            { "ip":"11.11.11.11", "timestamp":"3/12/2016 02:13:11", "amount": 9.25 },
            { "ip":"11.11.11.11", "timestamp":"3/12/2016 02:15:11", "amount": 2.25 },
            { "ip":"44.44.44.44", "timestamp":"3/13/2016 03:13:54", "amount": 8.75 },
            { "ip":"11.11.11.11", "timestamp":"3/14/2016 02:12:32", "amount": 6.50 },
            { "ip":"11.11.11.11", "timestamp":"3/14/2016 02:13:11", "amount": 9.25 },
            { "ip":"11.11.11.11", "timestamp":"3/14/2016 02:15:11", "amount": 2.25 },
            { "ip":"11.11.11.11", "timestamp":"3/15/2016 02:12:32", "amount": 6.50 },
            { "ip":"11.11.11.11", "timestamp":"3/16/2016 02:13:11", "amount": 9.25 },
            { "ip":"11.11.11.11", "timestamp":"3/16/2016 02:15:11", "amount": 2.25 },
            { "ip":"11.11.11.11", "timestamp":"3/16/2016 02:15:11", "amount": 2.25 },
        ];

        clickMonitor.output = [];
        let expectedResultClickLimit = [
            { "ip":"22.22.22.22", "timestamp":"3/11/2016 02:02:58", "amount": 7.00 },
            { "ip":"44.44.44.44", "timestamp":"3/13/2016 03:13:54", "amount": 8.75 }
        ]

        let resultClickLimit = clickMonitor.processClicks(testClickLimit)


        it('Ensure IP counts greater than 10 are excluded', () => {
            assert.deepEqual(expectedResultClickLimit, resultClickLimit);
        });

        ///////////  Test 3: Ensure empty click set returns empty  ////////////
        clickMonitor.output = [];
        let testEmptyClicks = [];

        let expectedResultEmptyClicks = [];

        let resultEmptyClicks = clickMonitor.processClicks(testEmptyClicks)

        it('Ensure empty click set returns empty', () => {
            assert.deepEqual(expectedResultEmptyClicks, resultEmptyClicks);
        })

    })
});

