import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import clicks from "./clicks"

// main reducers
export const reducers = combineReducers({
  routing: routerReducer,
  clicks: clicks
});
