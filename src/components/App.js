import React from "react";
import { connect } from "react-redux";
import { ProgressBar } from "react-bootstrap";
import ClickDisplay from "./ClickDisplay";

export class App extends React.Component {
  componentWillMount() {
    // the first time we load the app, we need that filtered clicks list
    this.props.dispatch({type: 'CLICKS_FETCH_LIST'});
  }
  render() {

    // show the loading state while we wait for the app to load
    const {clicks} = this.props;
    if (!clicks.length) {
      return (
        <ProgressBar active now={100}/>
      );
    }

    return (
      <div className="container">
        <h1>Clicks - Coding Challenge</h1>
        <h3>Requirements</h3>
        <p>Given an array of clicks, return the subset of clicks where:</p>
        <ol>
          <li>
              For each IP within each one hour period, only the most expensive click is placed into the result set.  
          </li>
          <li>If more than one click from the same IP ties for the most expensive click in a one hour period, only
              place the earliest click into the result set.</li>
          <li>
              If there are 10 or more clicks for an IP in the overall array of clicks, do not include any clicks from that
              IP address in the result set.
          </li>
        </ol>
        
        <h3>Answer</h3>
        <i>All logic for processing clicks is inside <b>middleware</b> folder
           and all clicks.json is stored in a mock api under <b>api</b> folder
        </i>
        {this.props.clicks.map((item,i) => 
              <ClickDisplay click={item} key={i} />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    clicks: state.clicks || [],
  };
}
export default connect(mapStateToProps)(App);