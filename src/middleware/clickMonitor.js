
import * as R from 'ramda'
import * as moment from 'moment'

class ClickMonitor {

    constructor(IPrepeatLimit = 0) {
      this.IPrepeatLimit = IPrepeatLimit;
      this._blackListedIPS = [];
      this.__currHourStart = null;
      this._nextHourStart = null;
      this.output = [];
    }

    processClicks(clicks){
        const moment = require('moment');

        //Ensure clicks is populated
        if (clicks === []){
            return [];
        }
        
        this._setIPblacklist(clicks);
        let {output, _blackListedIPS} = this;
    
        for (const click of clicks) {
            let lastItemInOutput = output[output.length - 1];
            let IPisBlackListed = click.ip in _blackListedIPS;

            if (!IPisBlackListed){
                //Only run on first iteration
                if(lastItemInOutput === undefined){
                    output.push(click);
                    this._setHourTrackers(click.timestamp);
                    continue;
                } 
                
                if(moment(click.timestamp).isAfter(this._nextHourStart)){
                    output.push(click);
                    this._setHourTrackers(click.timestamp);
                }else if (click.amount > lastItemInOutput.amount){
                    output.pop();
                    output.push(click);
                }
            }
        }
    
        return output;
        
    }
    
    //Track if click timestamp occurs in next or current hour
    _setHourTrackers(timeStamp){
        const moment = require('moment');

        if(timeStamp){
            this._currHourStart = moment(timeStamp).toDate();
            this._nextHourStart = moment(timeStamp).toDate();

            this._currHourStart.setMinutes(0);
            this._currHourStart.setSeconds(0);

            this._nextHourStart.setMinutes(59);
            this._nextHourStart.setSeconds(59);
        }    
    }

    //Group clicks by IPs then remove any that are over the IP limit
    _setIPblacklist(clicks){
        const {identity} = R;
        const groupByIP = R.groupBy(R.prop('ip'));

        const isBelowLimit = (n) => {
            return n.length < this.IPrepeatLimit;
        };

        //Keep only IPs whose counts are below the IP limit
        this._blackListedIPS = R.reject(isBelowLimit, groupByIP(clicks)); 
    }


}

export default ClickMonitor;


  